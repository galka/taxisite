__author__ = 'galka'

from django.conf.urls import patterns
from django.conf.urls import url
from mainapp import views

urlpatterns = patterns(
    '',
    url(r'^$', views.index, name='index'),
    url(r'^ride/$', views.ride, name='ride'),
    url(r'^new-ride/$', views.new_ride, name='new-ride'),
    url(r'^ride/view/(?P<pk>\d+)/$', views.view_ride, name='view_ride'),
    url(r'^ride/cancel/(?P<pk>\d+)/$', views.cancel_ride, name='cancel_ride'),
    url('^save_path/$', views.save_path, name="save-path"),
    url(r'^cancel_ride/$', views.cancel_ride, name='cancel_ride'),
)