from django.db import models
from django.conf import settings


class Driver(models.Model):
    driver_id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=254)
    is_active = models.BooleanField(null=False, blank=False, default=False, editable=True)

    def __unicode__(self):
        return self.name


class CabType(models.Model):
    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=254)

    def __unicode__(self):
        return u"%s" % self.name


class Cab(models.Model):
    id = models.AutoField(primary_key=True)
    licence_plate_number = models.IntegerField(unique=True)
    type = models.ForeignKey(CabType)
    is_active = models.BooleanField(null=False, blank=False, default=False, editable=True)

    def __unicode__(self):
        return u"%s" % self.id + self.type.name


class Status(models.Model):
    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=255, unique=True)

    def __unicode__(self):
        return self.name


class Ride(models.Model):
    ride_id = models.AutoField(primary_key=True, verbose_name='Id')
    eta = models.FloatField(verbose_name='Estimated time of arrival')
    calculated_distance = models.FloatField(verbose_name='Calculated distance')
    status_id = models.ForeignKey(Status, verbose_name='Status')
    driver_id = models.ForeignKey(Driver, verbose_name='Driver', blank=True, null=True)
    cab_id = models.ForeignKey(Cab, verbose_name='Cab', blank=True, null=True)
    login_id = models.ForeignKey(settings.AUTH_USER_MODEL, verbose_name='Login')
    notes = models.TextField(blank=True)
    time_created = models.DateTimeField(auto_now_add=True, verbose_name='When created')

    def __unicode__(self):
        return u"%s" % self.ride_id

    class Meta:
        ordering = ['-time_created']


class Address(models.Model):
    id = models.AutoField(primary_key=True)
    ride_id = models.ForeignKey(Ride)
    address = models.TextField()
    latitude = models.FloatField()
    longitude = models.FloatField()
    number_in_route = models.IntegerField()

    def __unicode__(self):
        return self.address