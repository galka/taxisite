__author__ = 'galka'

from django import forms
from models import Address
from models import Ride


class ChooseAddressForm(forms.Form):
    content = forms.CharField(max_length=256)
    created_at = forms.DateTimeField()


class AddressForm(forms.Form):
    address = forms.CharField()


class AddressSaveForm(forms.ModelForm):

    class Meta:
        model = Address


class RideSaveForm(forms.ModelForm):

    class Meta:
        model = Ride
        exclude = ('login_id', 'status_id',)


class RideForm(forms.ModelForm):

    class Meta:
        model = Ride
        exclude = ('login_id', 'calculated_distance', 'eta', 'status_id', 'driver_id', 'cab_id')


class RideFormUserAdmin(forms.ModelForm):

    class Meta:
        model = Ride
        exclude = ('login_id', 'calculated_distance', 'eta', 'status_id')


class RideViewForm(forms.ModelForm):

    class Meta:
        model = Ride
        exclude = ('login_id',)