__author__ = 'galka'

import django_tables2 as tables
from django_tables2.utils import A
from django_tables2.utils import AttributeDict
from django.utils.html import escape
from django.utils.safestring import mark_safe
from models import Address
from models import Ride


class ImageLinkColumn(tables.LinkColumn):
    def render_link(self, uri, text, attrs=None):
        attrs = AttributeDict(attrs if attrs is not None else
                              self.attrs.get('a', {}))
        attrs['href'] = uri
        html = '<a {attrs}>{text}</a>'.format(
            attrs=attrs.as_html(),
            text=text
        )
        return mark_safe(html)


class RideTable(tables.Table):
    addresses = ImageLinkColumn('', verbose_name='Addresses',
                                empty_values=(), default='')
    view = ImageLinkColumn('mainapp.views.view_ride', args=[A('pk')], verbose_name=' ',
                           empty_values=(), default="View",
                           attrs={"style": "text-align:center", "class": "btn btn-default btn-xs"})
    cancel = ImageLinkColumn('mainapp.views.cancel_ride', args=[A('pk')], verbose_name=' ',
                             empty_values=(), default="Cancel",
                             attrs={"style": "color:white;text-align:center", "class": "btn btn-danger btn-xs"})

    def render_view(self, value, record, column, bound_column):
        return column.render("<span class=\"glyphicon glyphicon-eye-open\"></span>"+column.default, record,
                             bound_column)

    def render_eta(self, value, record, column, bound_column):
        return "{0:.2f}".format(value/60) + " minutes"

    def render_calculated_distance(self, value, record, column, bound_column):
        return str(value) + " km"

    def render_cancel(self, value, record, column, bound_column):
        if record.status_id.name == 'Accepted':
            return column.render("<span class=\"glyphicon glyphicon-remove\"></span>"+column.default, record, bound_column)
        else:
            return mark_safe("")

    def render_addresses(self, value, record, column, bound_column):
        address_list = Address.objects.filter(ride_id=record).order_by('number_in_route')
        address_string = u''
        for idx, address in enumerate(address_list):
            current = '<strong>' + unicode(idx+1) + '</strong>' + ') ' + escape(address.address) \
                + " (lat: " + escape(address.latitude) + "; lng: " + escape(address.longitude) + ")<br><br>"
            address_string += current
        return mark_safe(address_string)

    class Meta:
        model = Ride
        attrs = {"class": "paleblue"}
        exclude = ("login_id", )
        orderable=False