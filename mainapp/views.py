import json
from django.contrib.auth.decorators import login_required
from django.contrib.auth.views import password_reset
from django.core.urlresolvers import reverse
from django.http import HttpResponse
from django.shortcuts import render_to_response
from django.shortcuts import redirect
from django_tables2 import RequestConfig
from django.template import RequestContext
from django.views.decorators.csrf import csrf_exempt
from forms import RideForm
from forms import RideSaveForm
from forms import RideViewForm
from forms import RideFormUserAdmin
from models import Address
from models import Ride
from tables import RideTable
from models import Status
from taxisite import settings


@login_required
def index(request):
    return redirect(reverse('ride'))


@login_required
def ride(request):
    table = RideTable(Ride.objects.filter(login_id=request.user.id))
    RequestConfig(request, paginate={"per_page": settings.MAX_RIDES_PER_PAGE}).configure(table)
    return render_to_response('geolocation/ride.html', {'table': table, }, RequestContext(request))


@login_required
def new_ride(request):
    form = RideForm()
    if request.user.is_superuser:
        form = RideFormUserAdmin()
    return render_to_response('geolocation/new_ride.html', {'form': form },
                              RequestContext(request))


@csrf_exempt
@login_required
def save_path(request):
    if request.method == 'POST' and request.is_ajax():
        form = RideSaveForm(request.POST or None)
        new_ride_row = form.save(commit=False)
        addresses = request.POST.getlist('addresses[]')
        lats = request.POST.getlist('lats[]')
        lngs = request.POST.getlist('lngs[]')
        errors = {}
        if not form.is_valid():
            return HttpResponse(json.dumps(form.errors), status=400)
        new_ride_row.login_id = request.user
        if request.user.is_superuser:
            if new_ride_row.driver_id and new_ride_row.cab_id:
                status_obj, created = Status.objects.get_or_create(name='Dispatched')
                new_ride_row.status_id = status_obj
            else:
                status_obj, created = Status.objects.get_or_create(name='Accepted')
                new_ride_row.status_id = status_obj
                if not new_ride_row.driver_id:
                   new_ride_row.cab_id = None
                else:
                 new_ride_row.driver_id = None
        else:
            status_obj, created = Status.objects.get_or_create(name='Accepted')
            new_ride_row.status_id = status_obj
        print new_ride_row
        new_ride_row.save()
        if not (len(addresses) == len(lats) == len(lngs)):
            errors += "check addresses"
        else:
            for i in range(0, len(lats)):
                address_instance = Address()
                address_instance.address = addresses[i]
                address_instance.latitude = lats[i]
                address_instance.longitude = lngs[i]
                address_instance.number_in_route = i
                address_instance.ride_id = new_ride_row
                address_instance.save()
        result = {}
        url = request.build_absolute_uri(reverse('ride'))
        result['redirect_url'] = url
        return HttpResponse(json.dumps(result), status=200)
    else:
        return HttpResponse(status=404)


@login_required
def view_ride(request, pk=0):
    obj = Ride.objects.get(ride_id=pk)
    if request.method == 'GET':
        if obj:
            form = RideViewForm()
            can_cancel = False
            if obj.status_id.name == 'Accepted':
                can_cancel = True
            address_list = Address.objects.filter(ride_id=obj).order_by('number_in_route')
            latlng_list = []
            for address in address_list:
                latlng = {}
                latlng['lat'] = address.latitude
                latlng['lng'] = address.longitude
                latlng_list.append(latlng)
            cost = (settings.INITIAL_PRICE_UA_HRN + obj.calculated_distance*settings.UA_HRN_PER_KM);

            ride_obj = [(Ride._meta.get_field_by_name('time_created')[0].verbose_name, obj.time_created),
                        (Ride._meta.get_field_by_name('status_id')[0].verbose_name, obj.status_id),
                        (Ride._meta.get_field_by_name('calculated_distance')[0].verbose_name, str(round(obj.calculated_distance, 2)) + " km"),
                        (Ride._meta.get_field_by_name('eta')[0].verbose_name, str(round(obj.eta/60, 2)) + " minutes"),
                        (Ride._meta.get_field_by_name('cab_id')[0].verbose_name, obj.cab_id),
                        (Ride._meta.get_field_by_name('driver_id')[0].verbose_name, obj.driver_id),
                        (Ride._meta.get_field_by_name('notes')[0].verbose_name, obj.notes)
            ]
            return render_to_response('geolocation/view_ride.html',
                                      {'addresses': address_list, 'ride_id': pk, 'can_cancel': can_cancel,
                                       'ride_obj': ride_obj, 'latlng_list': latlng_list,
                                       'cost': cost},
                                      RequestContext(request))
    elif request.method == 'POST':
        if obj:
            if obj.status_id.name == 'Accepted':
                status_cancel, created = Status.objects.get_or_create(name='Canceled')
                obj.status_id = status_cancel
                obj.save()
    return redirect(reverse('ride'))


@login_required
def cancel_ride(request, pk=0):
    obj = Ride.objects.get(ride_id=pk)
    if obj:
        if obj.status_id.name == 'Accepted':
            status_cancel, created = Status.objects.get_or_create(name='Canceled')
            obj.status_id = status_cancel
            obj.save()
    return redirect(reverse('ride'))


def global_vars(request):
    return {'API_KEY_MAP_QUEST': settings.API_KEY_MAP_QUEST,
            'MAX_INPUTS': settings.MAX_INPUTS,
            'UA_HRN_PER_KM': settings.UA_HRN_PER_KM,
            'INITIAL_PRICE_UA_HRN': settings.INITIAL_PRICE_UA_HRN,
            'VIEW_BOX_LEFT': settings.VIEW_BOX_LEFT,
            'VIEW_BOX_RIGHT': settings.VIEW_BOX_RIGHT,
            'VIEW_BOX_TOP': settings.VIEW_BOX_TOP,
            'VIEW_BOX_BOTTOM': settings.VIEW_BOX_BOTTOM,
            'COUNTRY_CODE': settings.COUNTRY_CODE,
            'PREFERRED_AREA': settings.PREFERRED_AREA
            }


def forgot_password(request):
    if request.method == 'POST':
        email = request.POST.get('email')
        return password_reset(request,
                              from_email=email)
    else:
        return render_to_response('registration/forgot_password.html',
                                  RequestContext(request))