from django.contrib import admin
from models import Cab
from models import CabType
from models import Driver
from models import Ride
from models import Status


class DriverAdmin(admin.ModelAdmin):
    pass
admin.site.register(Driver, DriverAdmin)


class CabTypeAdmin(admin.ModelAdmin):
    pass
admin.site.register(CabType, CabTypeAdmin)


class CabAdmin(admin.ModelAdmin):
    pass
admin.site.register(Cab, CabAdmin)


class StatusAdmin(admin.ModelAdmin):
    pass
admin.site.register(Status, StatusAdmin)


class RideAdmin(admin.ModelAdmin):
    def has_add_permission(self, request):
        return False
admin.site.register(Ride, RideAdmin)