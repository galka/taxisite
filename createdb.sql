DROP DATABASE IF EXISTS `taxisitedb`;

CREATE DATABASE `taxisitedb` DEFAULT CHARACTER SET utf8 DEFAULT COLLATE utf8_general_ci;

USE 'mysql';

create user 'taxisite_user' identified by '2014';

grant all  on taxisitedb.* to 'taxisite_user';

flush privileges;
