Setup project

*   install mysql client, start it, create db, tables according to DATABASES section in taxisite/settings.py

*   install python 2 (2.7) and packages listed in requirements.txt.

    You can use pip and virtual environment with python installed:

> mkdir py_env2_7
> cd py_env2_7
> sudo virtualenv2 --distribute --no-site-packages py_env2_7
> cd  py_env2_7
...
in project folder:
> source ../environment/py_env2_7/py_env2_7/bin/activate
> sudo pip install -r requirements.txt

*   create superuser for site admin who can add, delete and modify users

> python manage.py createsuperuser

*   syncdb
> python manage.py syncdb

*   run server
> python manage.py runserver

