/**
 * Created by galyna.butovych on 12.05.14.
 * map id is 'galynabutovych.i7gf9566' if needed
 */
var map;
var ajaxRequest;
var plotlist;
var plotlayers=[];

function initmap() {
	// set up the map
	map = new L.Map('map');
	// create the tile layer with correct attribution
	var osmUrl='http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png';
	var osmAttrib='Map data © <a href="http://openstreetmap.org">OpenStreetMap</a> contributors';
	var osm = new L.TileLayer(osmUrl, {minZoom: 8, maxZoom: 12, attribution: osmAttrib});

	// start the map in South-East England
//	map.setView(new L.LatLng( 39.125272, -76.900787 ),9);
//    map.locate({setView:true});
	map.addLayer(osm);
    var mapLayer = MQ.mapLayer();
    map.addLayer(mapLayer);
    L.control.layers({
        'Map': mapLayer,
        'Satellite': MQ.satelliteLayer(),
        'Hybrid': MQ.hybridLayer()
    }).addTo(map);
//    --------------------------------------------------------------
//    MQ.geocode({ map: map })
//        .search('lancaster pa');
//    MQ.geocode({ map: map })
//        .search('baltimore, md');
//    var dir;

//    dir = MQ.routing.directions();
//    dir.route({
//        locations: [
//            'baltimore, md',
//            'dulles, va'
//        ]
//    });
//    map.addLayer(MQ.routing.routeLayer({
//        directions: dir,
//        fitBounds: true
//    }));
}


