"""
Django settings for taxisite project.

For more information on this file, see
https://docs.djangoproject.com/en/1.6/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/1.6/ref/settings/
"""

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
import os
BASE_DIR = os.path.dirname(os.path.dirname(__file__))

# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/1.6/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = '1we=8id+#p2xxw)^1$j*x6y=-hz&xyxw4f20ptta&yq*!!lsox'

PASSWORD_HASHERS = (
    'django.contrib.auth.hashers.MD5PasswordHasher',
)

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

TEMPLATE_DEBUG = True

ALLOWED_HOSTS = []

# Application definition

INSTALLED_APPS = (
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'django.contrib.sites',
    'django.views.generic',
    'mainapp',
    'registration',
    'django_tables2'
)

MIDDLEWARE_CLASSES = (
    'django.middleware.common.CommonMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'django.middleware.locale.LocaleMiddleware',
)

AUTHENTICATION_BACKENDS = (
    'django.contrib.auth.backends.RemoteUserBackend',
    'django.contrib.auth.backends.ModelBackend',
)

ROOT_URLCONF = 'taxisite.urls'

WSGI_APPLICATION = 'taxisite.wsgi.application'

TEMPLATE_CONTEXT_PROCESSORS = (
    "django.core.context_processors.csrf",
    "django.contrib.auth.context_processors.auth",
    'django.core.context_processors.request',
    'django.core.context_processors.i18n',
    'mainapp.views.global_vars',
)

SESSION_ENGINE = 'django.contrib.sessions.backends.signed_cookies'

# Database
# https://docs.djangoproject.com/en/1.6/ref/settings/#databases

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql',
        #'NAME': os.path.join(BASE_DIR, 'taxisitedb'),
        'NAME': 'taxisitedb',
        'USER': 'taxisite_user',
        'PASSWORD': '2014',
        'HOST': '',
        'PORT': '',
    }
}

# Internationalization
# https://docs.djangoproject.com/en/1.6/topics/i18n/

LANGUAGE_CODE = 'en-us'

TIME_ZONE = 'UTC'

USE_I18N = True

USE_L10N = True

USE_TZ = True

ACCOUNT_ACTIVATION_DAYS = 2

AUTH_USER_MODEL = 'auth.User'

LOGIN_REDIRECT_URL = '/mainapp/ride/'

LOGIN_URL = '/login/'

LOGOUT_URL = '/logout/'

REGISTRATION_OPEN = False

EMAIL_BACKEND = "django.core.mail.backends.smtp.EmailBackend"
EMAIL_USE_TLS = True
EMAIL_HOST = 'smtp.gmail.com'
EMAIL_HOST_USER = 'gb.fake.mail'
EMAIL_HOST_PASSWORD = 'taxisite1111'
EMAIL_PORT = 587
DEFAULT_FROM_EMAIL = 'gb.fake.mail@gmail.com'

STATIC_PATH = os.path.join(BASE_DIR, 'static')

STATIC_URL = '/static/'

TEMPLATE_DIRS = (
    os.path.join(BASE_DIR, 'templates'),
)

STATICFILES_DIRS = (
    STATIC_PATH,
)

LOCALE_PATHS = (
    os.path.join(BASE_DIR, 'locale'),
)

SITE_ID = 1
gettext = lambda s: s
LANGUAGES = (
    ('uk', gettext('Ukrainian')),
    ('en', gettext('English')),
)

LANGUAGE_CODE = 'en'

# Custom settings:

MAX_RIDES_PER_PAGE = 20

# Custom credentials:
API_KEY_MAP_QUEST = 'Fmjtd%7Cluur2du8n5%2Can%3Do5-9ar5uw'

API_KEY_NAME_MAP_QUEST = 'bonvoyage'
MAX_INPUTS = 10
UA_HRN_PER_KM = 3.5
INITIAL_PRICE_UA_HRN = 10

# bounding box http://www.openstreetmap.org/export#map=11/49.8406/24.0951
VIEW_BOX_LEFT = 23.8184
VIEW_BOX_TOP = 49.9384
VIEW_BOX_RIGHT = 24.1898
VIEW_BOX_BOTTOM = 49.7582

PREFERRED_AREA = 'Lviv'

COUNTRY_CODE = 'ua'